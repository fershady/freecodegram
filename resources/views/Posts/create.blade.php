<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add New Post') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <form method="POST" action="{{route('post.store')}}" enctype="multipart/form-data" class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-4">
            @csrf
            <!-- Caption -->
            <div>
               <x-label for="caption" :value="__('Post Caption')"/>

               <x-input id="caption" class="block mt-1 w-full" type="text"  name="caption" :value="old('caption')" />
           </div>

           <!-- Image -->
           <div>
               <x-label for="image" :value="__('Image')"/>

               <x-input id="image" class="block mt-1 w-full" type="file" name="image" required/>
           </div>

           <x-button> {{__('Add New Post')}} </x-button>
        </form>
    </div>
</x-app-layout>
