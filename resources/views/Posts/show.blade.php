<x-app-layout >
    <div class="flex m-2 justify-center ">
        <div class="grid grid-cols-3 shadow max-w-screen-lg">
            <div class="col-span-2">
                <img src="/storage/{{$post->image}}" class="w-100">
            </div>
            <div class="m-6 space-y-2">
                <div class="flex space-x-1 pb-4 items-center border-b">
                    <img src="{{$post->user->profile->profileImage()}}" class="w-8 h-8 rounded-full">
                    <h3 class="font-bold">
                        <a href="/profile/{{$post->user->id}}">{{$post->user->username}}</a> •
                    </h3>
                    <x-label for="follow" :value="__('Follow')"/>
                </div>
                <div class="flex">
                    <p><span class="font-bold"><a href="/profile/{{$post->user->id}}">{{$post->user->username}}</a></span>
                    {{$post->caption}}</p>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
