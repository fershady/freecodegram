<x-app-layout>
    <div class="container max-w-7xl mx-auto sm:p-6 lg:p-8">
        <div class="flex m-2 justify-center">
            <div class="flex-none mr-12 md:mr-28 lg:px-8">
                <img src="{{$user->profile->profileImage()}}" class="rounded-full w-40" >
            </div>
            <section class="flex-auto max-w-2xl">
                <div class="flex justify-between items-baseline">
                    <h1 class="text-4xl">{{ $user->username}}</h1>
                    @can('update', $user->profile)
                        <a href="{{route('post.create')}}" class="text-sm text-gray-700 underline">Add New Post</a>
                    @endcan
                </div>

                @can('update', $user->profile)
                    <div><a href="/profile/{{$user->id}}/edit" class="text-sm text-gray-700 underline">Edit Profile</a></div>
                @endcan

                <div class="inline-flex space-x-5">
                    <div class="pr-5"><strong>{{$user->posts->count() }}</strong> posts</div>
                    <div class="pr-5"><strong>317</strong> following</div>
                    <div class="pr-5"><strong>73.4k</strong> followers</div>
                 </div>

                <div class="pt-4">{{$user->profile->title}}</div>
                <div>{{$user->profile->description}}</div>
                <div><a href="#">{{ $user->profile->url }}</a></div>
            </section>
        </div>

        <div class="grid grid-cols-3 gap-2 pt-6">
            @foreach ($user->posts as $post)
            <div>
                <a href="/p/{{$post->id}}">
                    <img src="/storage/{{$post->image}}" class="w-100">
                </a>
            </div>
            @endforeach
        </div>
    </div>
</x-app-layout>



{{-- <a href="{{ url('/p /createeeee') }}" class="text-sm text-gray-700 underline">Add New Post</a> --}}
