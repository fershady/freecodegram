<x-app-layout>
    <div class="flex flex-col sm:justify-center items-center pt-6 sm:pt-0">
        <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form action="/profile/{{ $user->id }}" enctype="multipart/form-data" method="post">
                @csrf
                @method('patch')

                <!-- Title -->
                <div>
                    <x-label for="title" :value="__('Title')"/>

                    <x-input id="title" class="block mt-1 w-full" type="text"  name="title" :value="old('title') ?? $user->profile->title"  required/>
                </div>

                <!-- Description -->
                <div>
                    <x-label for="description" :value="__('Description')"/>

                    <x-input id="description" class="block mt-1 w-full" type="text"  name="description" :value="old('description') ?? $user->profile->description"/>
                </div>

                <!-- Url -->
                <div>
                    <x-label for="url" :value="__('Url')"/>

                    <x-input id="url" class="block mt-1 w-full" type="text"  name="url" :value="old('url') ?? $user->profile->url" />
                </div>

                <!--Profile Image -->
                <div>
                    <x-label for="image" :value="__('Profile image')"/>

                    <x-input id="image" class="block mt-1 w-full" type="file"  name="image" :value="old('image')" />
                </div>

                <x-button> {{__('Save')}} </x-button>
            </form>
         </div>
    </div>
</x-app-layout>
