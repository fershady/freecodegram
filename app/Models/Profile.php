<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $guarded = [];

    public function profileImage()
    {
        return ($this -> image) ? '/storage/'. $this->image : '/image/default-portrait-image-generic-profile.jpg';
    }

    public function user()
    {
        return $this->belongsTo(User::class);

    }
}
