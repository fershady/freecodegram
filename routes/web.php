<?php

use App\Http\Controllers\PostsController;
use App\Http\Controllers\ProfilesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

that code   ->name('dashboard')     it's  the resource name.
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

 Route::view('profiles', 'welcome');
require __DIR__.'/auth.php';

Route::get('/user/{id}/{comment?}', function ($id,$comment='rojo') {
    return 'User '.$id.$comment;
}) ->where('id','[0-9]+') ->where('comment','[A-Za-z]+');

/* Post routes */
Route::get('/p/create', [PostsController::class, 'create'])->middleware('auth')->name('post.create');
Route::post('/p', [PostsController::class, 'store'])->middleware('auth')->name('post.store');
Route::get('/p/{post}', [PostsController::class, 'show'])->middleware('auth')->name('post.show');

/* Profile routes */
Route::get('/profile', function () {
    return 'profile request';
})->middleware(['auth'])->name('dashboard');;
Route::get('/profile/{user}', [ProfilesController::class, 'index'])->middleware(['auth'])->name('profile.show');
Route::get('/profile/{user}/edit', [ProfilesController::class, 'edit'])->middleware(['auth'])->name('profile.edit');
Route::patch ('/profile/{user}', [ProfilesController::class, 'update'])->middleware(['auth'])->name('profile.update');
